cfetch
======

Neofetch rewritten in c with speed in mind


(Keep in mind im a beginner in c and this is my first real project so dont expect much good code)


Usage:
	1. Rename `config.def.h` to `config.h`, to compile properly.

	2. Choose what compiler you want to use, Currently the Makefile supports clang, tcc, and gcc.

	3. Edit config.h to your preferences.

	4. add/remove features as you like. Feel free to create a pull request!

Contributing:
	You can contribute in many ways, The main way is to implement distro: packagemanagers, or even the package manger implementation in general.

	You can also help out with adding more ascii arts for distros.
