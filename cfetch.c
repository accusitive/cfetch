#include "config.h"

#define use_sysinfo defined(show_uptime) || defined(show_memory)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/utsname.h>
#if use_sysinfo
#include <sys/sysinfo.h>
#endif
#define check(os, pac_manager) if(strcmp(os_id, os) == 0){ \
  strcpy(package_manager, pac_manager); \
} 
char padding[64];
char os[256];
char os_id[256];
char package_manager[128];
int packages_installed = 0;
char key_col_fmt[64];
char val_col_fmt[64];

int count_widest(char *ascii_art) {
  int widest = 0; // Widest line
  char *copy = malloc(strlen(ascii_art) +
                      1); // Create copy so original ascii_art isnt reuined
  strcpy(copy, ascii_art);

  char *token = strtok(copy, "\n");
  while (token != NULL) {
    if (strlen(token) > widest) {
      widest = strlen(token);
    }
    token = strtok(NULL, " ");
  }
  free(copy);
  return widest;
}
int get_packages() {
  check("arch\n", "pacman");
  check("manjaro\n", "pacman");
  check("debian\n", "dpkg");
  check("ubuntu\n", "dpkg");
  check("alpine\n", "apk");
  check("\"void\"\n", "xbps");
  if (strcmp(package_manager, "dpkg") == 0) {
    FILE *fp;
    fp = popen("dpkg-query -W -f '\n'", "r");
    if (fp == NULL) {
      printf("Failed to get packages\n");
      exit(1);
    }
    while (fgets(os, sizeof(os), fp) != NULL) {
      packages_installed++;
    }
  }
  if (strcmp(package_manager, "apk") == 0) {
    FILE *fp;
    fp = popen("apk list -I", "r");
    if (fp == NULL) {
      printf("Failed to get packages\n");
      exit(1);
    }
    while (fgets(os, sizeof(os), fp) != NULL) {
      packages_installed++;
    }
  }

  if (strcmp(package_manager, "pacman") == 0) {
    FILE *fp;
    fp = popen("pacman -Q", "r");
    if (fp == NULL) {
      printf("Failed to get packages\n");
      exit(1);
    }
    while (fgets(os, sizeof(os), fp) != NULL) {
      packages_installed++;
    }
  }
  if(strcmp(package_manager, "xbps") == 0){
    FILE *fp;
    fp = popen("xbps-query -l", "r");
    if(fp == NULL){
      printf("Failed to get packages\n");
      exit(1);    
     }
     while(fgets(os, sizeof(os), fp) != NULL) {
        packages_installed++;
       }
  }
  return packages_installed;
}
char *get_os() { 

  FILE *fp;
  fp = popen("grep \"^ID=\" /etc/os-release | sed s/ID=//",
             "r"); 
  if (fp == NULL) {
    printf("Failed to get os\n");
    exit(1);
  }
  fgets(os_id, sizeof(os_id), fp);
  fgets(os, sizeof(os), fp);
  pclose(fp);
  return os_id;
}

int main() {
#ifdef key_color
  sprintf(key_col_fmt, "\x1B[38;5;%dm", key_color);
#else
  sprintf(key_col_fmt, "\x1B[38;5;%dm", 1);
#endif
#ifdef value_color
  sprintf(val_col_fmt, "\x1B[38;5;%dm", value_color);
#else
  sprintf(val_col_fmt, "\x1B[38;5;%dm", 5);
#endif
  char *reset_col_fmt = "\x1B[m";
  #ifndef force_os
  get_os();
  #else
  strcpy(os_id, force_os);
  #endif

  char ascii_art[512] = " \
     ___ \n\
    (.. | \n\
    (<> | \n\
   / __  \\ \n\
  ( /  \\ /| \n\
 _/\\ __)/_) \n\
 /-____/";     
  if(strcmp(os_id,"arch\n") == 0){
    strcpy(ascii_art,  "\
     /\\      \n\
    /  \\     \n\
   /\\   \\ \n\
  /      \\ \n\
 /   ,,   \\ \n\
/   |  |  -\\ \n\
/_-''    ''-_\\ \n\
			");
  }
   if(strcmp(os_id,"manjaro\n") == 0){
    strcpy(ascii_art,  "\
||||||||| |||| \n\
||||||||| |||| \n\
||||      |||| \n\
|||| |||| |||| \n\
|||| |||| |||| \n\
|||| |||| |||| \n\
|||| |||| |||| \n\
			");
  }
   if(strcmp(os_id,"ubuntu\n") == 0){
    strcpy(ascii_art,  "\
          _   \n\
      ---(_)\n\
  _/  ---  \\\n\
(_) |   |\n\
  \\  --- _/\n\
      ---(_)\n\
			");
  }
   if(strcmp(os_id,"debian\n") == 0){
    strcpy(ascii_art,  "\
    _____  \n\
    /  __ \\\n\
  |  /    |\n\
  |  \\___-\n\
  -_\n\
    --_\n\
			");
  }
                          // ascii art
  int ascii_h = 0;                       // ascii art height
  int ascii_w = count_widest(ascii_art); // ascii art width

  for (int i = 0; i < strlen(ascii_art); i++) // calculate ascii art height
  {
    if (ascii_art[i] == '\n') {
      ascii_h++;
    }
  }
  struct utsname unameData;
  int unameError = uname(&unameData);
  if (unameError != 0) {
    printf("uname Code error = %d\n", unameError);
  }
#if use_sysinfo
  struct sysinfo systemInfo;
  int sysinfoError = sysinfo(&systemInfo);

  if (sysinfoError != 0) {
    printf("sysinfo Code error = %d\n", sysinfoError);
  }

  int seconds = systemInfo.uptime;
  int minutes = seconds / 60;
  int hours = minutes / 60;
  unsigned long ram_used = (systemInfo.totalram - systemInfo.freeram) / 1024 / 1024;
  unsigned long swap_used = (systemInfo.totalswap - systemInfo.freeswap) / 1024 / 1024;
  unsigned long memory_used = ram_used + swap_used;
  unsigned long memory_max = systemInfo.totalram / 1024 / 1024;
#endif
  sprintf(padding, "\x1B[%iC", ascii_w); // Padding is a string that moves the
                                         // cursor `ascii_w` to the right.
  printf("%s", ascii_art);               // print ascii art
  printf("\x1B[%iA\n", ascii_h);         // move cursor up.
  printf("%s %s@%s\n", padding, getenv("USER"), unameData.nodename);
#ifdef show_kernel
  printf("%s %s%s:%s %s%s\n", padding, key_col_fmt, "kernel", reset_col_fmt,
         val_col_fmt, unameData.release);
#endif
#if defined(show_os) || defined(show_packages)
  printf("%s %s%s:%s %s%s", padding, key_col_fmt, "os", reset_col_fmt,
         val_col_fmt, get_os());
#endif
#ifdef show_memory
  printf("%s %s%s:%s %s%li/%li\n", padding, key_col_fmt, "memory",
         reset_col_fmt, val_col_fmt, memory_used, memory_max);
#endif
#ifdef show_uptime
  printf("%s %s%s:%s%s %d hrs, %d min\n", padding, key_col_fmt, "uptime",
         reset_col_fmt, val_col_fmt, hours, minutes % 60);
#endif
#ifdef show_packages
  printf("%s %s%s:%s %s%d", padding, key_col_fmt, "pkgs", reset_col_fmt,
         val_col_fmt, get_packages());
#endif
  printf("%s \n", padding); // so the prompt doesnt intersect with the logo
#ifdef show_mobo
  printf("mobo: test");
#endif
  return 0;
}
