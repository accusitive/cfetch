cfetch: cfetch.c config.h
	gcc -o cfetch cfetch.c 
optimize: cfetch.c config.h
	gcc -o cfetch cfetch.c -O3
clang: cfetch.c config.h
	clang -xc -Ofast -o cfetch cfetch.c
tcc: cfetch.c config.h
	tcc cfetch.c -o cfetch
mac: cfetch_mac.c config.h
	gcc -o cfetch cfetch_mac.c
install:
	rm /usr/bin/cfetch
	ln cfetch /usr/bin/cfetch
