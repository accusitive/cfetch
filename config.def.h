 
// // Uncomment to activate output from these 
// #define show_mobo
#define show_packages
#define show_kernel
#define show_os 
#define show_memory
#define show_uptime

/*
    0   -   black
    1   -   red 
    2   -   green
    3   -   yellow
    4   -   blue
    5   -   magenta
    6   -   cyan
    7   -   light gray
    8   -   dark gray
    9   -   bright red 
    10  -   bright green
    11  -   bright yellow
    12  -   bright blue
    13  -   bright pink
    14  -   bright cyan
    15  -   white
Add 8 to use bright colors.
https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit
*/
#define key_color 1 //1 - 8
#define value_color 15
// #define force_os "ubuntu\n" //Force ascii to use this logo.

